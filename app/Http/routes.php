<?php

// use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Route::get('test1', function(){
//	phpinfo();
//});

Route::get('queue', function(){
	$user = \App\User::find(14);
//	return $user;
	$user->name = 'Talis3';
	$user->save();
});


Route::get('/', [ 'middleware' => 'auth', function () {

	if(Auth::check()) {
		return redirect()->route('client.index');
	}
	return redirect()->route('login');

}]);



Route::get('test/{id}','Client\InvoiceController@calculateTotalInvoiceAmount');


Route::get('login', ['as' => 'login', 'uses' => 'HomeController@login']);
Route::get('logout', ['as' => 'logout', 'uses' => 'HomeController@logout']);


Route::get('login/facebook', ['as'=>'login.facebook','uses'=>'Auth\AuthController@redirectToFacebookProvider' ]);
Route::get('login/facebook/callback', 'Auth\AuthController@handleFacebookProviderCallback');

Route::get('login/linkedin', ['as'=>'login.linkedin','uses'=>'Auth\AuthController@redirectToLinkedinProvider']);
Route::get('login/linkedin/callback', 'Auth\AuthController@handleLinkedinProviderCallback');

Route::get('login/twitter', ['as'=>'login.twitter','uses'=>'Auth\AuthController@redirectToTwitterProvider']);
Route::get('login/twitter/callback', 'Auth\AuthController@handleTwitterProviderCallback');

Route::get('login/google', ['as'=>'login.google','uses'=>'Auth\AuthController@redirectToGoogleProvider']);
Route::get('login/google/callback', 'Auth\AuthController@handleGoogleProviderCallback');

require(app_path() . '/Http/Routes/clientRoutes.php');
require(app_path() . '/Http/Routes/adminRoutes.php');










// Route::resource('posts', 'TpostController');
//
//Route::group(['middleware' => 'web'], function () {
//	//var_dump(Route::auth());
//
//	Route::auth();
//
////    Route::get('/home', 'HomeController@index');
//	return redirect()->route('client.index');
//});
