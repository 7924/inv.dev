<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\App;

class ForClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }

        // dd(\App::make('Company')); 

        // ja ejam uz citiem routem izņemot client.companies.index - lai nebūtu redireckts loopā
        // un uz client.companies.show - kas, piešķir ID
        if(  \Request::route()->getName() != 'client.companies.index' && \Request::route()->getName() != 'client.companies.show'){


            // ja nav ID, tad redirekts uz client.companies.index
        	if( !isset( \App::make('Company')->id ) ){
                
                return redirect(route('client.companies.index') );
            }
        }
        return $next($request);
    }
}
