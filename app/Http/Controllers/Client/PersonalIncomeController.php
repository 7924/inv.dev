<?php

namespace App\Http\Controllers\Client;


use App\PersonalIncomeCostRate;
use App\PersonalIncomeTaxRate;
use App\PersonalIncomeType;
use App\Repositories\PersonalIncome\PersonalIncomeRepository;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use App\Partner;
use Validator;

class PersonalIncomeController extends Controller
{

    public $personalIncomes;

    public function __construct(PersonalIncomeRepository $personalIncomes)
    {
        $this->company = App::make('Company');
        if (!isset($this->company->id))
        {
            return route('client.companies.index');
        }
        $this->companyId = $this->company['id'];

        $this->personalIncomes = $personalIncomes;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $partners = $this->personalIncomes->getPartners();


        $personalIncomeTypes = $this->personalIncomes->getPersonalIncomeTypes();


        $params = $request->all();


        if (isset($params['filter']))
        {
            $filter = $params['filter'];
            foreach ($filter as $key => $value)
            {
                if (isset($filter[$key]))
                {
                    setcookie('cookies[' . $this->companyId . '][filter][' . $key . ']', $filter[$key], env('COOKIE_EXPIRE_TIME'));
                    $_COOKIE['cookies'][$this->companyId]['filter'][$key] = $filter[$key];
                }
            }
        }


        if (isset($params['sort']))
        {

            $sort = $params['sort'];
            foreach ($sort as $key => $value)
            {
                if (isset($sort[$key]))
                {
                    foreach ($sort[$key] as $k => $v)
                    {
                        setcookie('cookies[' . $this->companyId . '][sort][' . $key . '][' . $k . ']', $v, env('COOKIE_EXPIRE_TIME'));
                        $_COOKIE['cookies'][$this->companyId]['sort'][$key][$k] = $v;
                    }


                }
            }
        }


        $params = isset($_COOKIE['cookies'][$this->companyId]) ? $_COOKIE['cookies'][$this->companyId] : [];


        $personalIncomes = $this->personalIncomes->getPersonalIncomes($params);

        if (!$personalIncomes)
        {
//            return $this->index(new Request);
        }

        return view('client.personal-incomes.index', compact('personalIncomes', 'partners', 'params','personalIncomeTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partners = Partner::where('company_id', $this->companyId)->get()->pluck('name', 'id');
        $personalIncomeTypes = PersonalIncomeType::get()->pluck('name', 'id');
        $personalIncomeTaxRates = PersonalIncomeTaxRate::get();
        $personalIncomeCostRates = PersonalIncomeCostRate::get();

        return view('client.personal-incomes.create', compact('partners', 'personalIncomeTypes', 'personalIncomeTaxRates', 'personalIncomeCostRates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
