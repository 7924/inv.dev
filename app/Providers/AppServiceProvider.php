<?php

namespace App\Providers;

use App\Http\Requests\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\App;

use App\Company;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        

        App::bind('App\\Repositories\\Invoice\\InvoiceRepository', 'App\\Repositories\\Invoice\\EloquentInvoiceRepository');
        App::bind('App\\Repositories\\PersonalIncome\\PersonalIncomeRepository', 'App\\Repositories\\PersonalIncome\\EloquentPersonalIncomeRepository');


        app()->singleton('Company', function ($app) {

            if (\Session::has('companyId')) {
                return Company::orderBy('title', 'asc')->find(Session::get('companyId'));
            } else {
                return redirect(route('login'));
            }

        });


    }
}
